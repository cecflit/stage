***SuperTux Adventure Game Engine***<br>
Manual

**IMPORTING THE ENGINE**

- In all sectors of your level, import the engine in the init script `import("stage/stage.nut");`<br>
- After this line, you can set the name of the location and the colour it's displayed in: `Stage.set_screen_name("My Adventurous Location", "orange");`<br>
- As you can see, the function takes two variables, one for the name and one for the colour. Refer to *Simplified colours* at the end of this README to get more information about how colours can be defined in STAGE.
- Now, it's time to define the items you will be using in the level. You can do this on the fly when putting the items in the level, but it is highly recommended you do this in a separate file that you then import alongside the engine. You can define your items as such: `my_item <- Stage.StageItem("My Item", "levels/mylevels/myitem.sprite", "My item's description");`<br>
- The first string is an identifying name of the item. This determines where the item can be used - this is also the name of the item displayed in-game. The sprite determines what it looks like. The last string is the description which shows up when you examine the item from your inventory<br>
- Please note that the sprite must be 32×32. Other sizes will not fit the inventory slots properly!<br>
- After you define the items, it's time to run the engine. To do this, simply run `Stage.initialise();` in your "main" sector's init script, and `Stage.run();` in all other sectors' init scripts. DO NOT DO IT THE OTHER WAY AROUND! IT WILL NOT WORK!!!

**ADDING ITEMS**

- To add collectible items to your level, you will need to use Powerups. To add an item, put a Powerup to the level and set its sprite to your desired look. To turn it into a StageItem, you will also need to set up the following collect script: `Stage.collect_item(my_item);`.<br>
- Here, the items you set up in advance will come in handy. If you didn't do it, you have to write the script as so: `Stage.collect_item(Stage.StageItem("My Item", "levels/mylevels/myitem.sprite", "My item's description"));`<br>
- Alternatively, you can spawn items using the `Stage.spawn_item(item, x, y)` function. Refer to *Overview of all functions* near the end of this README for more information.

**USING ITEMS**

- To make an item useful, you will have to set up a function that defines what happens when you use the item at the appropriate location, and also tell the game where the appropriate location is.<br>
- To define the function, you can put it into a variable in the file you already made for defining your items. You can do it as follows: `myitem_reaction <- function() {sector.Tux.add_coins(100);};`<br>
- This particular function will give Tux 100 coins.<br>
- To tell the game where you can use an item, you will need to use scripttriggers. When you enter the location where you can use the desired item, run the following function: `Stage.set_accepted_item("My Item", myitem_reaction);`<br>
- This will set up which item that location reacts to, and how.<br>
- To reset this when you leave the location, run `Stage.clear_accepted_items();`.<br>
- It's also possible for one location to accept multiple items, each with a unique reaction. To achieve that, use `Stage.add_accepted_item("My Item", myitem_reaction);` for all of the items you wish to accept.<br>
- **Note:** To retain an item when it's used, you can simply add `true` as the third argument of `set_accepted_item()` or `add_accepted_item()`.

**ITEM RECIPES**

- STAGE allows you to create recipes for creating items. Each recipe always combines two items into a new one. Crafting is available from the inventory. To define a recipe, run `Stage.add_recipe("My Item", "My Other Item", resulting_item);` The used items are identified by their name, the resulting item is a StageItem you can define along with all your other items.

**MULTIPLE SECTORS**

- To avoid glitches, use the `Stage.teleport(sector, spawnpoint);` function to teleport between sectors, or the `Stage.door(sector, spawnpoint);` function for doors. If this isn't possible, run `Stage.leave_sector();` before changing sectors in another way.<br>
- Please note that `leave_sector()` and `Level.spawn()` will not prevent teleportation even when teleportation is disabled in Stage, which can lead to serious glitches. You should always call `teleport()` for all means of teleportation (or `door()` for doors), unless you have a very good reason not to and you know exactly what you are doing.<br>

**DIALOGUES**

- To create a dialogue, run `Stage.dialogue(**your lines go here**);`. The lines can either be simple strings, or arrays containing the string and the colour it should be displayed in, such as `["Hello, friends!", "purple"]`. Separate the lines with commas.<br>
- You can also define dialogues in your external file. To do this, create an array containing all of your lines. You can then hand this array directly to the `dialogue()` function.<br>
- If you wish to make a simple one-line dialogue with no special colours, just running `Stage.dialogue("Simple line");` will work. The default colour is white.

**MAPS**

- Using a map for your level allows the player to easily navigate it as well as fast travel, if you wish so.<br>
- By default, there is no map in a STAGE project and no button to open it.<br>
- If you wish to create a map, you will need to create individual map pieces. You can create a map piece using the `Stage.MapPiece(name, colour, image, sector, spawnpoint);` constructor.<br>
- `name` and `colour` should ideally be the same as the screen name of the sector this map piece depicts and its colour.<br>
- `image` is a path to a picture depicting this sector. The image's dimensions should be 192×192 pixels.<br>
- TIP: You can make the images slightly larger to create overlaps (e.g. for torn pieces of a map or jigsaw pieces). Remember that each image will be centred in its 192×192 space.<br>
- `sector` and `spawnpoint` point to a specific location for fast travel. If either of these is `null`, fast travel will be disabled for this map piece.<br>
- To add a map piece to the map, call `Stage.set_map_piece(piece, x, y);`. Here, `piece` refers to the MapPiece you have just created and `x` and `y` are its coordinates on the map. These can be anything between -128 and 127 on both axes. Generally, you want the origin to be your main sector.<br>
- Once a map piece is added to the map, the map becomes active and can be accessed using the ITEM POCKET key from the inventory.<br>
- By default, when the map is opened, it will open at the location of the last added map piece in the current sector. You can override this by calling `Stage.set_map_position(x, y);`. This is especially useful for sectors that are not necessarily present on the map, such as insides of buildings.<br>
- If no map pieces were added in a sector and `Stage.set_map_position(x, y);` has not been called, the map will open at the location of the last map piece added to it in *any* sector.

**OVERVIEW OF ALL FUNCTIONS**<br>
*This includes additional functions not mentioned above*<br>

- `Stage.initialise();` - Run this in your `main` sector's init script.<br>
- `Stage.run();` - Run this in the init script of all other sectors.<br>
- `Stage.save_inventory(inventory_id);` - Saves the contents of the inventory. You can trigger this function at the end of each level if your world spans multiple. You can also specify an id (string) for your save. This way, you can have multiple saved inventories, for, for example, different sub-worlds of your worldmap. The default id is "default", which automatically loads at the beginning of each level.<br>
- `Stage.load_inventory(inventory_id);` - Restores contents of the inventory to the saved state identified by its id (string). This destroys all items currently in the inventory. Inventory "default" is loaded automatically at the beginning of each level. You can counter this by calling this function with a different id. This function can be called before `Stage.initialise();`/`Stage.run();`<br>
- `Stage.collect_inventory(inventory_id);` - Adds all items from the saved inventory identified by `inventory_id` to the current inventory. This keeps all items currently in the inventory intact.<br>
- `Stage.set_screen_name(name, colour);` - Sets the display name of the current sector to `name`, and displays it in the specified colour. This function can be called before `Stage.initialise();`/`Stage.run();`.<br>
- `Stage.set_tips(tips);` - Sets the tips displayed on the bottom when the inventory is open. Takes an array of strings, or just strings as separate arguments. Giving it an empty array will disable tips altogether. This function can be called before `Stage.initialise();`/`Stage.run();`.<br>
- `Stage.add_recipe(ingredient1, ingredient2, result);` - Creates a crafting recipe. Order of ingredients does not matter. Ingredients are identified by their identifying names (strings), the result is a StageItem. This function can be called before `Stage.initialise();`/`Stage.run();`.<br>
- `Stage.add_accepted_item(item_name, reaction, retain);` - Tells STAGE that if item identified by its name is used from the inventory, it should trigger function `reaction()` instead of being dropped. `retain` is a bool that determines whether or not the item should be retained in the inventory when it's used. The default value is `false`.<br>
- `Stage.remove_accepted_item(item_name);` - Tells STAGE that the specified item should no longer be accepted, and dropped instead if used from the inventory.<br>
- `Stage.clear_accepted_items();` - Tells STAGE that no items shall be accepted at the moment. Using any item from the inventory will just drop it.<br>
- `Stage.set_accepted_item(item_name, reaction, retain);` - Like `add_accepted_item()`, but it also clears any other pre-existing acceptances.<br>
- `Stage.add_examine_function(item_name, function);` - Makes the 'examine' function in the inventory call the specified function instead of showing the item's description. Useful for reading books. Works on a sector-by-sector basis - it is recommended you call this function in your external `.nut` file. This function can be called before `Stage.initialise();`/`Stage.run();`.<br>
- `Stage.remove_examine_function(item_name);` - Resets the 'examine' functionality for the specified item to showing its description. Works on a sector-by-sector basis.<br>
- `Stage.clear_examine_functions();` - Resets the 'examine' functionality for all items to showing their description. Works on a sector-by-sector basis.<br>
- `Stage.add_collect_function(item_name, function);` - Tells STAGE to trigger the specified function when item, identified by its name, is collected. This function can be called before `Stage.initialise();`/`Stage.run();`.<br>
- `Stage.remove_collect_function(item_name);` - Resets the collect function for the specified item.<br>
- `Stage.clear_collect_functions();` - Resets the collect functions for all items.<br>
- `Stage.add_drop_function(item_name, function);` - Tells STAGE to trigger the specified function when item, identified by its name, is dropped. This function can be called before `Stage.initialise();`/`Stage.run();`.<br>
- `Stage.remove_drop_function(item_name);` - Resets the drop function for the specified item.<br>
- `Stage.clear_drop_functions();` - Resets the drop functions for all items.<br>
- `Stage.add_prevent_drop_function(item_name, function);` - Prevents the item identified by its name from being dropped from the inventory. Triggers the specified function every time the player attempts to drop the item. This function can be called before `Stage.initialise();`/`Stage.run();`.<br>
- `Stage.remove_prevent_drop_function(item_name);` - Allows the player to drop the specified item if this action was previously prevented.<br>
- `Stage.clear_prevent_drop_functions();` - Allows all items that were prevented from being dropped to be dropped again.<br>
- `Stage.add_craft_function(item_name, function);` - Tells STAGE to trigger the specified function when item, identified by its name, is crafted. This function can be called before `Stage.initialise();`/`Stage.run();`.<br>
- `Stage.remove_craft_function(item_name);` - Resets the craft function for the specified item.<br>
- `Stage.clear_craft_functions();` - Resets the craft functions for all items.<br>
- `Stage.inventory_contains(item_name);` - Returns true if there is an item of the specified name in the player's inventory, returns false otherwise.<br>
- `Stage.steal_from_inventory(item_name);` - Removes all items of the corresponding name from the player's inventory.<br>
- `Stage.replace_in_inventory(item_name, new_item);` - Replaces all instances of item identified by its name by the specified new item.<br>
- `Stage.inventory_empty();` - Returns true if there are no items in the inventory. Returns false otherwise.<br>
- `Stage.empty_inventory();` - Removes all items from the player's inventory.<br>
- `Stage.items_in_inventory([item_name]);` - Returns how many items there are in the player's inventory. If `item_name` is provided, it returns how many instances of item identified by `item_name` there are in the inventory.<br>
- `Stage.unique_items_in_inventory();` - Returns how many unique items (meaning items of different names) there are in the player's inventory.<br>
- `Stage.spawn_item(item, x, y);` - Spawns a StageItem at coordinates x, y of the current sector.<br>
- `Stage.collect_item(item);` - Puts a StageItem in the player's inventory.<br>
- `Stage.leave_sector();` - Unloads assets before leaving a sector. This is needed to avoid graphical glitches.<br>
- `Stage.teleport(sector, spawnpoint);` - Teleports the player to the specified location while also running `leave_sector()` for you. This function also prevents glitches connected to teleporting while in inventory, dialogue or book mode.<br>
- `Stage.door(sector, spawnpoint);` - Teleports the player to the specified location like `teleport()` does, but adds a delay that corresponds to the time it takes the door-opening animation to play.<br>
- `Stage.dialogue(dialogue);` - Presents the player with a dialogue. The dialogue is a list of lines, where each line can be either a string, or an array consisting of the string and a colour. The list can either be handed to the function as arguments, or as an array.<br>
- `Stage.live_caption(text, colour, position, time);` - Shows a caption without deactivating Tux. Calling this function does NOT hold the vm for the specified time. Allowed positions are "left", "middle", and "right". Captions can also be defined in arrays, the function can read them too.<br>
- `Stage.book(book_title, [title, image, text], ...)` - Opens a book which the player can list through. `book_title` only shows in the status bar, `title` and `text` can either be strings, or arrays `[string, colour]`. `image` is a string determining the path to the illustration. Any of these can be null. All arguments can be in an array.<br>
- `Stage.picture(picture_title, file_path)` - Opens an image the player can view. Useful for viewing maps and similar items.<br>
- `Stage.menu(title, [entry_name, entry_reaction], [entry_name, entry_reaction], ...)` - Opens an interactive menu. `title` is a string, each entry is an array `[entry_name, entry_reaction]`, where `entry_name` is a string and `entry_reaction` is a function that gets called if that menu entry is selected. All of the parameters can be in an array.<br>
- `Stage.update_status(new_status);` - Sets what the status bar shows.<br>
- `Stage.allow_teleport(allow)` - If set to false, any `Stage.teleport()` call will return without teleporting.<br>
- `Stage.allow_inventory(allow)` - If set to false, the player will not be able to access their inventory until reset to true. Useful during cutscenes.<br>
- `Stage.set_map_piece(piece, x, y)` - Sets the map piece at map coordinates x, y to the MapPiece represented by `piece`. Activates the map if it is the first piece being set up.<br>
- `Stage.set_map_position(x, y);` - Sets the position at which the map should open in the current sector to x, y.<br>
- `Stage.MapPiece(name, colour, image, sector, spawnpoint);` - Creates a MapPiece with the specified name, (text) colour and image. The image must be 192×192 pixels. `sector` and `spawnpoint` point to a specific location for fast travel. If either is set to null, fast travel for this MapPiece will be disabled.<br>
- `Stage.StageItem(name, sprite, description);` - Returns a StageItem of the specified name, sprite and description. This item can then be used with other functions, such as `spawn_item()`, `collect_item()`, `add_recipe()` and more. This constructor can be called before `Stage.initialise();`/`Stage.run();`.<br>
- `Stage.enable_multithreading(allow)` - You can use this function before `Stage.initialise`/`Stage.run` to disable multithreading. This will cause Stage to hold the init-script's vm. You may want to try this option if you encounter bugs with your item functions.<br>

**SIMPLIFIED COLOURS**

- When giving a colour to STAGE, you can either do it in this format: `[r, g, b]`, where r, g and b are the RGB values, or, more simply, by using a string naming the colour. The following colour names are recognised by STAGE:<br>
- `"red"`        == `[1, 0, 0]`<br>
- `"green"`      == `[0, 1, 0]`<br>
- `"blue"`       == `[0, 0, 1]`<br>
- `"darkblue"`   == `[0, 0, 0.5]`<br>
- `"darkgreen"`  == `[0, 0.5, 0]`<br>
- `"lightblue"`  == `[0, 0.6, 1]`<br>
- `"lime"`       == `[0.68, 1, 0.18]`<br>
- `"brick"`      == `[0.5, 0, 0]`<br>
- `"crimson"`    == `[1, 0.25, 0.4]`<br>
- `"purple"`     == `[0.7, 0, 1]`<br>
- `"violet"`     == `[0.57, 0.44, 0.86]`<br>
- `"orange"`     == `[1, 0.5, 0]`<br>
- `"yellow"`     == `[1, 1, 0]`<br>
- `"gold"`       == `[0.85, 0.64, 0.125]`<br>
- `"turquoise"`  == `[0, 1, 1]`<br>
- `"teal"`       == `[0, 0.5, 0.5]`<br>
- `"pink"`       == `[1, 0.3, 1]`<br>
- `"brown"`      == `[0.5, 0.25, 0]`<br>
- `"lightbrown"` == `[0.6, 0.5, 0.4]`<br>
- `"olive"`      == `[0.5, 0.5, 0]` <br>
- `"magenta"`    == `[1, 0, 1]` <br>
- `"beige"`      == `[0.96, 0.87, 0.7]`<br>
- `"sky"`        == `[0.53, 0.8, 0.92]`<br>
- `"white"`      == `[1, 1, 1]`<br>
- `"black"`      == `[0, 0, 0]`<br>
- `"grey"`       == `[0.5, 0.5, 0.5]`<br>
- `"darkgrey"`   == `[0.25, 0.25, 0.25]`<br>
- `"silver"`     == `[0.75, 0.75, 0.75]`
