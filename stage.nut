//SuperTux Adventure Game Engine
//Created by Narre

Stage <- {};
Stage.screen_name <- "The Stage";
Stage.screen_name_colour <- [1, 1, 1];
Stage.inventory_background_image <- "/stage/images/inventory.png";
Stage.workbench_background_image <- "/stage/images/workbench.png";
Stage.recipes <- {};
Stage.examine_functions <- {};
Stage.collect_functions <- {};
Stage.prevent_drop_functions <- {};
Stage.drop_functions <- {};
Stage.craft_functions <- {};
Stage.dialogue_open <- false;
Stage.inventory_source <- "default";
Stage.initialised <- false;
Stage.multithreading <- true;
Stage.skip_inventory_tp <- false;
if(!("stage" in Level)) {
	Level.stage <- {};
	Level.stage.stage_id <- null;
	Level.stage.map <- null;
	Level.stage.map_position <- [0, 0];
}

Stage.tips <- [
	"You can use UP to interact with NPCs and some objects.",
	"If your inventory is full, leave all redundant items in one place, so you can find them later.",
	"You can examine items to help you figure out their function.",
	"You are immune to damage when browsing the inventory or reading a dialogue."
];

Stage.initialise <- function() {
	if(!("init_script_run" in sector)) {
		sector.init_script_run <- true;
		if(!("stage" in state)) state.stage <- {};
		if(!("saved-inventories" in state.stage)) state.stage["saved-inventories"] <- {};
		if(!("default" in state.stage["saved-inventories"])) {
			state.stage["saved-inventories"]["default"] <- {};
			for(i <- 0; i < 12; ++i) state.stage["saved-inventories"]["default"]["s" + i] <- false;
		}
		state.stage.inventory <- {};
		state.stage["status-bar-text"] <- _("Press ENTER for your inventory");
		for(i <- 0; i < 12; ++i) state.stage.inventory["s" + i] <- state.stage["saved-inventories"][inventory_source]["s" + i];
		Level.stage <- {};
		Level.stage.map <- null;
		Level.stage.map_position <- [0, 0];
	}
	Stage.run();
}

Stage.run_thread <- function(instance_id) {
	while(true) {
		if(sector.Tux.get_input_held("menu-select") && !sector.Stage.dialogue_open) Stage.open_inventory();
		try {
			if(!("Stage" in sector) || Level.stage.stage_id != instance_id) return;
		} catch(e) {
			return;
		}
		if(Stage.left_caption_on) {
			if(Stage.left_caption_timer <= 0) {
				Stage.left_caption_on <- false;
				Stage.left_caption.set_visible(false);
			} else Stage.left_caption_timer -= (1.0/60);
		}
		if(Stage.right_caption_on) {
			if(Stage.right_caption_timer <= 0) {
				Stage.right_caption_on <- false;
				Stage.right_caption.set_visible(false);
			} else Stage.right_caption_timer -= (1.0/60);
		}
		if(Stage.mid_caption_on) {
			if(Stage.mid_caption_timer <= 0) {
				Stage.mid_caption_on <- false;
				Stage.mid_caption.set_visible(false);
			} else Stage.mid_caption_timer -= (1.0/60);
		}
		wait(0.01);
		try {
			if(!("Stage" in sector) || Level.stage.stage_id != instance_id) return;
		} catch(e) {
			return;
		}
	}
}

Stage.run <- function() {
	Level.stage.stage_id <- rand();

	Effect.sixteen_to_nine(0);
	Stage.inventory_background <- FloatingImage(Stage.inventory_background_image);
	Stage.inventory_background.set_layer(1000);
	Stage.inventory_background.set_anchor_point(ANCHOR_BOTTOM);
	Stage.inventory_background.set_pos(0, -2);
	Stage.inventory_background.set_visible(true);

	Stage.workbench_background <- FloatingImage(Stage.workbench_background_image);
	Stage.workbench_background.set_layer(1000);
	Stage.workbench_background.set_anchor_point(ANCHOR_BOTTOM_RIGHT);
	Stage.workbench_background.set_pos(-32, -21);
	Stage.workbench_background.set_visible(true);

	Stage.screen_name_indicator <- TextObject();
	Stage.screen_name_indicator.set_wrap_width(sector.Camera.get_screen_width() - 32);
	Stage.screen_name_indicator.set_centered(true);
	Stage.screen_name_indicator.set_text(Stage.screen_name);
	Stage.screen_name_indicator.set_anchor_point(ANCHOR_TOP);
	Stage.screen_name_indicator.set_font("big");
	Stage.screen_name_colour <- Stage.get_colour(Stage.screen_name_colour);
	Stage.screen_name_indicator.set_text_color(Stage.screen_name_colour[0], Stage.screen_name_colour[1], Stage.screen_name_colour[2], 1);
	Stage.screen_name_indicator.set_pos(0, 16);
	Stage.screen_name_indicator.set_back_fill_color(1, 1, 1, 0);
	Stage.screen_name_indicator.set_front_fill_color(1, 1, 1, 0);
	Stage.screen_name_indicator.set_visible(true);

	Stage.status_bar <- TextObject();
	Stage.status_bar.set_wrap_width(sector.Camera.get_screen_width() / 2 - 124);
	Stage.status_bar.set_text(state.stage["status-bar-text"]);
	Stage.status_bar.set_anchor_point(ANCHOR_BOTTOM_LEFT);
	Stage.status_bar.set_pos(8, -17);
	Stage.status_bar.set_front_fill_color(1, 1, 1, 0);
	Stage.status_bar.set_back_fill_color(1, 1, 1, 0);
	Stage.status_bar.set_visible(true);

	Stage.left_caption <- TextObject();
	Stage.left_caption.set_wrap_width(sector.Camera.get_screen_width() / 3 - 36);
	Stage.left_caption.set_centered(true);
	Stage.left_caption.set_anchor_point(ANCHOR_LEFT);
	if(sector.Camera.get_screen_height() >= 800) Stage.left_caption.set_pos(24, -128);
	else Stage.left_caption.set_pos(24, -64);
	Stage.left_caption.set_back_fill_color(1, 1, 1, 0);
	Stage.left_caption.set_roundness(9);
	Stage.left_caption_timer <- 0;
	Stage.left_caption_on <- false;

	Stage.right_caption <- TextObject();
	Stage.right_caption.set_wrap_width(sector.Camera.get_screen_width() / 3 - 36);
	Stage.right_caption.set_centered(true);
	Stage.right_caption.set_anchor_point(ANCHOR_RIGHT);
	if(sector.Camera.get_screen_height() >= 800) Stage.right_caption.set_pos(-24, -128);
	else Stage.right_caption.set_pos(-24, -64);
	Stage.right_caption.set_back_fill_color(1, 1, 1, 0);
	Stage.right_caption.set_roundness(9);
	Stage.right_caption_timer <- 0;
	Stage.right_caption_on <- false;

	Stage.mid_caption <- TextObject();
	Stage.mid_caption.set_wrap_width(sector.Camera.get_screen_width() / 3 - 36);
	Stage.mid_caption.set_centered(true);
	if(sector.Camera.get_screen_height() >= 800) Stage.mid_caption.set_pos(0, -172);
	else Stage.mid_caption.set_pos(0, -96);
	Stage.mid_caption.set_back_fill_color(1, 1, 1, 0);
	Stage.mid_caption.set_roundness(9);
	Stage.mid_caption_timer <- 0;
	Stage.mid_caption_on <- false;

	Stage.accept_items <- {};
	Stage.workbench <- {s0 = false, s1 = false, s2 = false};

	Stage.skip_sound <- false;
	Stage.prevent_teleport <- false;
	Stage.inventory_open <- false;

	if(!("inventory_item_renderer" in Stage)) sector.Stage.inventory_item_renderer <- {};
	if(!("workbench_item_renderer" in Stage)) Stage.workbench_item_renderer <- {};

	for(i <- 0; i < 12; ++i) {
		if(state.stage.inventory["s" + i]) {
			sector.Stage.inventory_item_renderer["s" + i] <- FloatingImage(state.stage.inventory["s" + i].sprite);
			Stage.render_inventory_item(i);
		} else sector.Stage.inventory_item_renderer["s" + i] <- FloatingImage("/stage/images/empty.png");
	}
	for(i <- 0; i < 3; ++i) Stage.workbench_item_renderer["s" + i] <- FloatingImage("/stage/images/empty.png");

	Stage.initialised <- true;

	if(Stage.multithreading) {
		stage_instance <- newthread(Stage.run_thread);
		stage_instance.call(Level.stage.stage_id);
	} else {
		Stage.run_thread(Level.stage.stage_id);
	}
}

Stage.set_map_piece <- function(mp, x, y) {
	if(x < -128 || x > 127 || y < -128 || y > 127) {
		print("[ERROR] Map piece coordinates out of bounds!");
		return;
	}
	if(!Level.stage.map) {
		Level.stage.map = array(256);
		for(i <- 0; i < 256; ++i) {
			Level.stage.map[i] = array(256);
			for(j <- 0; j < 256; ++j) Level.stage.map[i][j] = null;
		}
		Level.stage.map_extremes <- {};
		Level.stage.map_extremes.left <- x;
		Level.stage.map_extremes.right <- x;
		Level.stage.map_extremes.top <- y;
		Level.stage.map_extremes.bottom <- y;
	}

	Level.stage.map[x + 128][y + 128] = mp;
	if(x < Level.stage.map_extremes.left) Level.stage.map_extremes.left = x;
	else if(x > Level.stage.map_extremes.right) Level.stage.map_extremes.right = x;
	if(y < Level.stage.map_extremes.top) Level.stage.map_extremes.top = y;
	else if(y > Level.stage.map_extremes.bottom) Level.stage.map_extremes.bottom = y;
	Level.stage.map_position = [x, y];
}

Stage.set_map_position <- function(x, y) {
	if(!Level.stage.map) {
		print("[ERROR] Map not initialised!");
		return;
	}
	if(x < Level.stage.map_extremes.left ||
	   x > Level.stage.map_extremes.right ||
	   y < Level.stage.map_extremes.top ||
	   y > Level.stage.map_extremes.bottom) {
		print("[ERROR] Map position outside of map boundaries!");
		return;
	}
	Level.stage.map_position = [x, y];
}

Stage.save_inventory <- function(save_id = "default") {
	state.stage["saved-inventories"][save_id] <- {};
	for(ix <- 0; ix < 12; ++ix) state.stage["saved-inventories"][save_id]["s" + ix] <- state.stage.inventory["s" + ix];
}

Stage.load_inventory <- function(save_id = "default") {
	if(!(save_id in state.stage["saved-inventories"])) {
		state.stage["saved-inventories"][save_id] <- {};
		for(ix <- 0; ix < 12; ++ix) state.stage["saved-inventories"][save_id]["s" + ix] <- false;
	}
	if(Stage.initialised) {
		Stage.empty_inventory();
		for(ix <- 0; ix < 12; ++ix) {
			if(!state.stage["saved-inventories"][save_id]["s" + ix]) continue;
			state.stage.inventory["s" + ix] <- state.stage["saved-inventories"][save_id]["s" + ix];
			sector.Stage.inventory_item_renderer["s" + ix] <- FloatingImage(state.stage.inventory["s" + ix].sprite);
			Stage.render_inventory_item(ix);
		}
	} else {
		Stage.inventory_source <- save_id;
	}
}

Stage.collect_inventory <- function(save_id = "default") {
	if(!(save_id in state.stage["saved-inventories"])) {
		state.stage["saved-inventories"][save_id] <- {};
		for(ix <- 0; ix < 12; ++i) state.stage["saved-inventories"][save_id]["s" + i] <- false;
	}
	for(ix <- 0; ix < 12; ++ix) {
		if(!state.stage["saved-inventories"][save_id]["s" + ix]) continue;
		Stage.collect_item(state.stage["saved-inventories"][save_id]["s" + ix]);
	}
}

Stage.update_status <- function(status) {
	state.stage["status-bar-text"] <- status;
	sector.Stage.status_bar.set_text(status);
}

Stage.get_colour <- function(colour) {
	switch(colour) {
		case "red": return [1, 0, 0];
		case "green": return [0, 1, 0];
		case "blue": return [0, 0, 1];
		case "darkblue": return [0, 0, 0.5];
		case "darkgreen": return [0, 0.5, 0];
		case "lightblue": return [0, 0.6, 1];
		case "lime": return [0.68, 1, 0.18];
		case "brick": return [0.5, 0, 0];
		case "crimson": return [1, 0.25, 0.4];
		case "purple": return [0.7, 0, 1];
		case "violet": return [0.57, 0.44, 0.86];
		case "orange": return [1, 0.5, 0];
		case "yellow": return [1, 1, 0];
		case "gold": return [0.85, 0.64, 0.125];
		case "turquoise": return [0, 1, 1];
		case "teal": return [0, 0.5, 0.5];
		case "pink": return [1, 0.4, 1];
		case "brown": return [0.5, 0.25, 0];
		case "lightbrown": return [0.6, 0.5, 0.4];
		case "olive": return [0.5, 0.5, 0];
		case "magenta": return [1, 0, 1];
		case "beige": return [0.96, 0.87, 0.7];
		case "sky": return [0.53, 0.8, 0.92];
		case "white": return [1, 1, 1];
		case "black": return [0, 0, 0];
		case "grey": return [0.5, 0.5, 0.5];
		case "darkgrey": return [0.25, 0.25, 0.25];
		case "silver": return [0.75, 0.75, 0.75];
		default:
			if(typeof colour == "array") return colour;
			return [1, 1, 1];
	}
}

Stage.set_tips <- function(...) {
	if(typeof vargv[0] == "array") Stage.tips <- vargv[0];
	else Stage.tips <- vargv;
}

Stage.add_examine_function <- function(item_name, examine) {
	Stage.examine_functions[item_name] <- examine;
}

Stage.remove_examine_function <- function(item_name) {
	if(item_name in Stage.examine_functions) Stage.examine_functions[item_name] <- null;
}

Stage.clear_examine_functions <- function() {
	Stage.examine_functions <- {};
}

Stage.add_collect_function <- function(item_name, collect) {
	Stage.collect_functions[item_name] <- collect;
}

Stage.remove_collect_function <- function(item_name) {
	if(item_name in Stage.collect_functions) Stage.collect_functions[item_name] <- null;
}

Stage.clear_collect_functions <- function() {
	Stage.collect_functions <- {};
}

Stage.add_drop_function <- function(item_name, drop) {
	Stage.drop_functions[item_name] <- drop;
}

Stage.remove_drop_function <- function(item_name) {
	if(item_name in Stage.drop_functions) Stage.drop_functions[item_name] <- null;
}

Stage.clear_drop_functions <- function() {
	Stage.drop_functions <- {};
}

Stage.add_prevent_drop_function <- function(item_name, prevent) {
	Stage.prevent_drop_functions[item_name] <- prevent;
}

Stage.remove_prevent_drop_function <- function(item_name) {
	if(item_name in Stage.prevent_drop_functions) Stage.prevent_drop_functions[item_name] <- null;
}

Stage.clear_prevent_drop_functions <- function() {
	Stage.prevent_drop_functions <- {};
}

Stage.add_craft_function <- function(item_name, craftf) {
	Stage.craft_functions[item_name] <- craftf;
}

Stage.remove_craft_function <- function(item_name) {
	if(item_name in Stage.craft_functions) Stage.craft_functions[item_name] <- null;
}

Stage.clear_craft_functions <- function() {
	Stage.craft_functions <- {};
}

Stage.set_screen_name <- function(name, colour) {
	Stage.screen_name <- _(name);
	Stage.screen_name_colour <- Stage.get_colour(colour);
	if("screen_name_indicator" in Stage) {
		Stage.screen_name_indicator.set_text(name);
		Stage.screen_name_indicator.set_text_color(
			Stage.screen_name_colour[0],
			Stage.screen_name_colour[1],
			Stage.screen_name_colour[2], 1
		);
	}
}

Stage.add_recipe <- function(ingredient1, ingredient2, result) {
	Stage.recipes[ingredient1 + "+" + ingredient2] <- result;
}

Stage.clear_accepted_items <- function() {
	Stage.accept_items <- {};
}

Stage.add_accepted_item <- function(item, reaction, retain = false) {
	Stage.accept_items[item] <- [reaction, retain];
}

Stage.set_accepted_item <- function(item, reaction, retain = false) {
	Stage.clear_accepted_items();
	Stage.add_accepted_item(item, reaction, retain);
}

Stage.remove_accepted_item <- function(item) {
	if(!(item in Stage.accept_items)) return;
	Stage.accept_items[item] <- null;
}

Stage.dialogue <- function(...) {
	if(!vargv.len()) return;

	Stage.prevent_teleport <- true;
	Stage.left_caption.set_visible(false);
	Stage.right_caption.set_visible(false);
	Stage.mid_caption.set_visible(false);
	Stage.left_caption_timer <- 0;
	Stage.right_caption_timer <- 0;
	Stage.mid_caption_timer <- 0;
	Stage.left_caption_on <- false;
	Stage.right_caption_on <- false;
	Stage.mid_caption_on <- false;

	if(vargv.len() == 1 && typeof vargv[0] == "array" &&
	   !(vargv[0].len() == 2 && typeof vargv[0][1] == "array" && vargv[0][1].len() == 3)) {
		texts <- array(vargv[0].len());
		for(i <- 0; i < texts.len(); ++i) texts[i] = array(2);

		for(i <- 0; i < vargv[0].len(); ++i) {
			if(typeof vargv[0][i] == "string") {
				texts[i][0] = vargv[0][i];
			} else {
				texts[i][0] = vargv[0][i][0];
				if(vargv[0][i].len() > 1) texts[i][1] = vargv[0][i][1];
			}
		}
	} else {
		texts <- array(vargv.len());
		for(i <- 0; i < texts.len(); ++i) texts[i] = array(2);

		for(i <- 0; i < vargv.len(); ++i) {
			if(typeof vargv[i] == "string") {
				texts[i][0] = vargv[i];
			} else {
				texts[i][0] = vargv[i][0];
				if(vargv[i].len() > 1) texts[i][1] = vargv[i][1];
			}
		}
	}

	Stage.dialogue_open <- true;
	sector.Tux.deactivate();

	Stage.dialogue_prompt <- TextObject();
	Stage.dialogue_prompt.set_wrap_width(sector.Camera.get_screen_width() - 8);
	Stage.dialogue_prompt.set_anchor_point(ANCHOR_BOTTOM);
	Stage.dialogue_prompt.set_pos(0, -76);
	Stage.dialogue_prompt.set_roundness(0);
	Stage.dialogue_prompt.set_back_fill_color(0.2, 0.2, 0.2, 1);
	Stage.dialogue_prompt.set_text(_("Press UP or ACTION to continue"));

	Stage.dialogue_textbox <- TextObject();
	Stage.dialogue_textbox.set_roundness(0);
	Stage.dialogue_textbox.set_back_fill_color(0.2, 0.2, 0.2, 1);
	Stage.dialogue_textbox.set_text(_(texts[0][0]));
	init_colour <- Stage.get_colour(texts[0][1]);
	Stage.dialogue_textbox.set_text_color(init_colour[0], init_colour[1], init_colour[2], 1);
	Stage.dialogue_textbox.set_visible(true);
	Stage.dialogue_prompt.set_visible(true);

	foreach(text in texts) {
		Stage.dialogue_textbox.set_text(_(text[0]));
		colour <- Stage.get_colour(text[1]);
		Stage.dialogue_textbox.set_text_color(colour[0], colour[1], colour[2], 1);

		while(sector.Tux.get_input_held("jump") || sector.Tux.get_input_held("up") || sector.Tux.get_input_held("action")) wait(0.01);
		while(!sector.Tux.get_input_held("jump") && !sector.Tux.get_input_held("up") && !sector.Tux.get_input_held("action")) wait(0.01);
	}

	Stage.dialogue_textbox.set_text("");
	Stage.dialogue_textbox.set_visible(false);
	Stage.dialogue_prompt.set_text("");
	Stage.dialogue_prompt.set_visible(false);

	while(sector.Tux.get_input_held("jump") || sector.Tux.get_input_held("up") || sector.Tux.get_input_held("action")) wait(0.01);
	sector.Tux.activate();
	Stage.dialogue_open <- false;
	Stage.prevent_teleport <- false;
}

Stage.book <- function(...) {
	if(vargv.len() == 1) chapters <- vargv[0];
	else chapters <- vargv;

	sector.Tux.deactivate();

	Stage.left_caption.set_visible(false);
	Stage.right_caption.set_visible(false);
	Stage.mid_caption.set_visible(false);
	Stage.left_caption_timer <- 0;
	Stage.right_caption_timer <- 0;
	Stage.mid_caption_timer <- 0;
	Stage.left_caption_on <- false;
	Stage.right_caption_on <- false;
	Stage.mid_caption_on <- false;

	Stage.prevent_teleport <- true;
	Stage.dialogue_open <- true;

	Stage.book_background <- FloatingImage("/stage/images/book.png");
	Stage.book_background.set_layer(600);
	Stage.book_background.set_pos(15, -5);
	Stage.book_background.set_visible(true);

	Stage.pinfo <- TextObject();
	Stage.pinfo.set_anchor_point(ANCHOR_BOTTOM);
	Stage.pinfo.set_pos(0, -76);
	Stage.pinfo.set_roundness(0);
	Stage.pinfo.set_back_fill_color(0.2, 0.2, 0.2, 1);
	Stage.pinfo.set_text(format(_("Page %i/%i"), 1, chapters.len() - 1));
	Stage.pinfo.set_visible(true);

	Stage.pleft <- TextObject();
	Stage.pleft.set_anchor_point(ANCHOR_BOTTOM);
	Stage.pleft.set_pos(-180, -76);
	Stage.pleft.set_text(_("[LEFT]"));
	Stage.pleft.set_text_color(0.4, 0.4, 0.4, 1);
	Stage.pleft.set_roundness(0);
	Stage.pleft.set_back_fill_color(0.2, 0.2, 0.2, 1);
	Stage.pleft.set_visible(true);

	Stage.pright <- TextObject();
	Stage.pright.set_anchor_point(ANCHOR_BOTTOM);
	Stage.pright.set_pos(180, -76);
	Stage.pright.set_roundness(0);
	Stage.pright.set_back_fill_color(0.2, 0.2, 0.2, 1);
	Stage.pright.set_text(_("[RIGHT]"));
	if(chapters.len == 1) Stage.pright.set_text_color(0.4, 0.4, 0.4, 1);
	Stage.pright.set_visible(true);

	Stage.pclose <- TextObject();
	Stage.pclose.set_wrap_width(sector.Camera.get_screen_width() - 8);
	Stage.pclose.set_anchor_point(ANCHOR_TOP_LEFT);
	Stage.pclose.set_pos(8, 17);
	Stage.pclose.set_back_fill_color(0.2, 0.2, 0.2, 0);
	Stage.pclose.set_front_fill_color(0.2, 0.2, 0.2, 0);
	Stage.pclose.set_text_color(1, 1, 0, 1);
	Stage.pclose.set_text(_("Press UP or ACTION to continue"));
	Stage.pclose.set_visible(true);

	Stage.ptitle <- TextObject();
	Stage.ptitle.set_wrap_width(400);
	Stage.ptitle.set_centered(true);
	Stage.ptitle.set_font("big");
	Stage.ptitle.set_pos(-220, -180);
	Stage.ptitle.set_front_fill_color(1, 1, 1, 0);
	Stage.ptitle.set_back_fill_color(1, 1, 1, 0);
	Stage.ptitle.set_visible(true);

	Stage.ptext <- TextObject();
	Stage.ptext.set_centered(true);
	Stage.ptext.set_wrap_width(400);
	Stage.ptext.set_pos(220, -30);
	Stage.ptext.set_front_fill_color(1, 1, 1, 0);
	Stage.ptext.set_back_fill_color(1, 1, 1, 0);
	Stage.ptext.set_visible(true);

	Stage.update_status(format(_("%s opened"), _(chapters[0])));
	Stage.page <- 1;

	while(sector.Tux.get_input_held("up") || sector.Tux.get_input_held("jump") || sector.Tux.get_input_held("action")) wait(0.01);

	while(true) {
		if(chapters[Stage.page][0]) {
			if(typeof chapters[Stage.page][0] == "string") {
				Stage.ptitle.set_text(_(chapters[Stage.page][0]));
				Stage.ptitle.set_text_color(1, 1, 1, 1);
			} else {
				Stage.ptitle.set_text(_(chapters[Stage.page][0][0]));
				tit_col <- Stage.get_colour(chapters[Stage.page][0][1]);
				Stage.ptitle.set_text_color(tit_col[0], tit_col[1], tit_col[2], 1);
			}
		} else Stage.ptitle.set_text("");

		if(chapters[Stage.page][1]) {
			Stage.pimage <- FloatingImage(chapters[Stage.page][1]);
			Stage.pimage.set_pos(-220, 0);
			Stage.pimage.set_layer(800);
			Stage.pimage.set_visible(true);
		}

		if(chapters[Stage.page][2]) {
			if(typeof chapters[Stage.page][2] == "string") {
				Stage.ptext.set_text(_(chapters[Stage.page][2]));
				Stage.ptext.set_text_color(1, 1, 1, 1);
			} else {
				Stage.ptext.set_text(_(chapters[Stage.page][2][0]));
				txt_col <- Stage.get_colour(chapters[Stage.page][2][1]);
				Stage.ptext.set_text_color(txt_col[0], txt_col[1], txt_col[2], 1);
			}
		} else Stage.ptext.set_text("");

		while(sector.Tux.get_input_held("right") || sector.Tux.get_input_held("left")) {
			wait(0.01);
			Stage.ptitle.set_visible(true);
			Stage.ptext.set_visible(true);
		}

		while(true) {
			if(sector.Tux.get_input_held("right") && Stage.page < chapters.len() - 1) {
				Stage.ptitle.set_visible(false);
				Stage.ptext.set_visible(false);
				if(chapters[Stage.page][1]) Stage.pimage.set_visible(false);

				Stage.pleft.set_text_color(1, 1, 1, 1);
				if(++Stage.page == chapters.len() - 1) Stage.pright.set_text_color(0.4, 0.4, 0.4, 1);

				Stage.pinfo.set_text(format(_("Page %i/%i"), Stage.page, chapters.len() - 1));
				play_sound("stage/sounds/page.ogg");
				break;
			} else if(sector.Tux.get_input_held("left") && Stage.page > 1) {
				Stage.ptitle.set_visible(false);
				Stage.ptext.set_visible(false);
				if(chapters[Stage.page][1]) Stage.pimage.set_visible(false);

				Stage.pright.set_text_color(1, 1, 1, 1);
				if(--Stage.page == 1) Stage.pleft.set_text_color(0.4, 0.4, 0.4, 1);

				Stage.pinfo.set_text(format(_("Page %i/%i"), Stage.page, chapters.len() - 1));
				play_sound("stage/sounds/page.ogg");
				break;
			} else if(sector.Tux.get_input_held("jump") || sector.Tux.get_input_held("up") || sector.Tux.get_input_held("action")) {
				Stage.ptitle.set_visible(false);
				Stage.ptext.set_visible(false);
				if(chapters[Stage.page][1]) Stage.pimage.set_visible(false);

				Stage.pclose.set_visible(false);
				Stage.pleft.set_visible(false);
				Stage.pright.set_visible(false);
				Stage.pinfo.set_visible(false);
				Stage.book_background.set_visible(false);
				Stage.update_status(format(_("%s closed"), _(chapters[0])));

				while(sector.Tux.get_input_held("jump") ||
				      sector.Tux.get_input_held("up") ||
				      sector.Tux.get_input_held("action")
				) wait(0.01);
				sector.Tux.activate();
				Stage.dialogue_open <- false;
				if(!Stage.inventory_open) Stage.prevent_teleport <- false;
				return;
			}
			wait(0.01);
		}
	}
}

Stage.picture <- function(pic_name, pic_path) {
	Stage.dialogue_open <- true;
	Stage.prevent_teleport <- true;
	sector.Tux.deactivate();

	Stage.update_status(format("%s viewed", _(pic_name)));

	Stage.picture_fo <- FloatingImage(pic_path);
	Stage.picture_fo.set_layer(980);
	Stage.picture_fo.set_visible(true);

	Stage.pclose <- TextObject();
	Stage.pclose.set_wrap_width(sector.Camera.get_screen_width() - 8);
	Stage.pclose.set_anchor_point(ANCHOR_TOP_LEFT);
	Stage.pclose.set_pos(8, 17);
	Stage.pclose.set_back_fill_color(0.2, 0.2, 0.2, 0);
	Stage.pclose.set_front_fill_color(0.2, 0.2, 0.2, 0);
	Stage.pclose.set_text_color(1, 1, 0, 1);
	Stage.pclose.set_text(_("Press UP or ACTION to continue"));
	Stage.pclose.set_visible(true);

	while(sector.Tux.get_input_held("up")) wait(0.01);

	while(!sector.Tux.get_input_held("jump") && !sector.Tux.get_input_held("up") && !sector.Tux.get_input_held("action")) wait(0.01);

	Stage.picture_fo.set_visible(false);
	Stage.pclose.set_visible(false);

	Stage.update_status(format("%s closed", _(pic_name)));

	sector.Tux.activate();
	Stage.dialogue_open <- false;
	Stage.prevent_teleport <- false;	
}

Stage.live_caption <- function(...) {
	if(vargv.len() == 1 && typeof vargv[0] == "array") caption <- vargv[0];
	else caption <- vargv;

	c_col <- Stage.get_colour(caption[1]);

	switch(caption[2]) {
		case "left":
			Stage.left_caption.set_text_color(c_col[0], c_col[1], c_col[2], 1);
			Stage.left_caption.set_text(_(caption[0]));
			Stage.left_caption.set_visible(true);
			Stage.left_caption_timer <- caption[3];
			Stage.left_caption_on <- true;
			break;
		case "right":
			Stage.right_caption.set_text_color(c_col[0], c_col[1], c_col[2], 1);
			Stage.right_caption.set_text(_(caption[0]));
			Stage.right_caption.set_visible(true);
			Stage.right_caption_timer <- caption[3];
			Stage.right_caption_on <- true;
			break;
		default:
			Stage.mid_caption.set_text_color(c_col[0], c_col[1], c_col[2], 1);
			Stage.mid_caption.set_text(_(caption[0]));
			Stage.mid_caption.set_visible(true);
			Stage.mid_caption_timer <- caption[3];
			Stage.mid_caption_on <- true;
			break;
	}
}

Stage.menu <- function(...) {
	if(vargv.len() == 1 && typeof vargv[0] == "array") entries <- vargv[0];
	else entries <- vargv;

	Stage.left_caption.set_visible(false);
	Stage.right_caption.set_visible(false);
	Stage.mid_caption.set_visible(false);
	Stage.left_caption_timer <- 0;
	Stage.right_caption_timer <- 0;
	Stage.mid_caption_timer <- 0;
	Stage.left_caption_on <- false;
	Stage.right_caption_on <- false;
	Stage.mid_caption_on <- false;

	Stage.menu_title <- TextObject();
	Stage.menu_title.set_wrap_width(10000);
	Stage.menu_title.set_centered(true);
	Stage.menu_title.set_roundness(0);
	Stage.menu_title.set_back_fill_color(0.35, 0.35, 0.4, 1);
	Stage.menu_title.set_front_fill_color(1, 1, 1, 0);
	Stage.menu_title.set_text_color(0, 0.6, 1, 1);
	str_tit <- _(entries[0]) + "\n***************";
	for(i <- 1; i < entries.len(); ++i) str_tit += "\n";
	Stage.menu_title.set_text(str_tit);
	Stage.menu_title.set_visible(true);

	Stage.menu_selected <- TextObject();
	Stage.menu_selected.set_wrap_width(10000);
	Stage.menu_selected.set_centered(true);
	Stage.menu_selected.set_roundness(0);
	Stage.menu_selected.set_back_fill_color(0.35, 0.35, 0.4, 1);
	Stage.menu_selected.set_front_fill_color(1, 1, 1, 0);
	Stage.menu_selected.set_text_color(1, 0.5, 0, 1);
	str_sel <- " \n\n" + _(entries[1][0]);
	for(i <- 2; i < entries.len(); ++i) str_sel += "\n";
	Stage.menu_selected.set_text(str_sel);
	Stage.menu_selected.set_visible(true);

	Stage.menu_unselected <- TextObject();
	Stage.menu_unselected.set_wrap_width(10000);
	Stage.menu_unselected.set_centered(true);
	Stage.menu_unselected.set_roundness(0);
	Stage.menu_unselected.set_back_fill_color(0.35, 0.35, 0.4, 1);
	Stage.menu_unselected.set_front_fill_color(1, 1, 1, 0);
	str_unsel <- " \n\n";
	for(i <- 2; i < entries.len(); ++i) str_unsel += ("\n" + _(entries[i][0]));
	Stage.menu_unselected.set_text(str_unsel);
	Stage.menu_unselected.set_visible(true);

	Stage.dialogue_open <- true;
	sector.Tux.deactivate();
	sel_menu_item <- 1;
	Stage.cycles_held <- 0;
	last_dir <- "";
	Stage.prevent_teleport <- true;

	while(true) {
		wait(0.01);

		if(!sector.Tux.get_input_held("up") && !sector.Tux.get_input_held("down")) Stage.cycles_held <- 0;

		if(sector.Tux.get_input_held("up")) {
			if(sel_menu_item == 1) continue;
			--sel_menu_item;
			if(last_dir == "down") Stage.cycles_held <- 0;
			last_dir <- "up";

			str_sel <- " \n";
			for(i <- 1; i < sel_menu_item; ++i) str_sel += "\n";
			str_sel += ("\n" + _(entries[sel_menu_item][0]));
			for(i <- sel_menu_item + 1; i < entries.len(); ++i) str_sel += "\n";
			Stage.menu_selected.set_text(str_sel);

			str_unsel <- " \n";
			for(i <- 1; i < sel_menu_item; ++i) str_unsel += ("\n" + _(entries[i][0]));
			str_unsel += "\n";
			for(i <- sel_menu_item + 1; i < entries.len(); ++i) str_unsel += ("\n" + _(entries[i][0]));
			Stage.menu_unselected.set_text(str_unsel);

			while(sector.Tux.get_input_held("up")) {
				wait(0.01);
				if(++Stage.cycles_held >= 40) break;
			}

		} else if(sector.Tux.get_input_held("down")) {
			if(sel_menu_item == entries.len() - 1) continue
			++sel_menu_item;
			if(last_dir == "up") Stage.cycles_held <- 0;
			last_dir <- "down";

			str_sel <- " \n";
			for(i <- 1; i < sel_menu_item; ++i) str_sel += "\n";
			str_sel += ("\n" + _(entries[sel_menu_item][0]));
			for(i <- sel_menu_item + 1; i < entries.len(); ++i) str_sel += "\n";
			Stage.menu_selected.set_text(str_sel);

			str_unsel <- " \n";
			for(i <- 1; i < sel_menu_item; ++i) str_unsel += ("\n" + _(entries[i][0]));
			str_unsel += "\n";
			for(i <- sel_menu_item + 1; i < entries.len(); ++i) str_unsel += ("\n" + _(entries[i][0]));
			Stage.menu_unselected.set_text(str_unsel);

			while(sector.Tux.get_input_held("down"))  {
				wait(0.01);
				if(++Stage.cycles_held >= 40) break;
			}

		} else if(sector.Tux.get_input_held("menu-select")) break;
	}
	Stage.menu_title.set_visible(false);
	Stage.menu_selected.set_visible(false);
	Stage.menu_unselected.set_visible(false);

	while(sector.Tux.get_input_held("menu-select")) wait(0.01);
	sector.Tux.activate();
	Stage.dialogue_open <- false;
	Stage.prevent_teleport <- false;
	entries[sel_menu_item][1]();
}

Stage.inventory_contains <- function(item_name) {
	foreach(item in state.stage.inventory) {
		if(item && item.name == item_name) return true;
	}
	return false;
}

Stage.steal_from_inventory <- function(item_name) {
	if(!Stage.inventory_contains(item_name)) return;
	for(i <- 0; i < 12; ++i) {
		if(state.stage.inventory["s" + i] && state.stage.inventory["s" + i].name == item_name) {
			sector.Stage.inventory_item_renderer["s" + i].set_visible(false);
			state.stage.inventory["s" + i] <- false;
		}
	}
}

Stage.replace_in_inventory <- function(item_name, new_item) {
	if(!Stage.inventory_contains(item_name)) return;
	for(i <- 0; i < 12; ++i) {
		if(state.stage.inventory["s" + i] && state.stage.inventory["s" + i].name == item_name) {
			state.stage.inventory["s" + i] <- new_item;
			sector.Stage.inventory_item_renderer["s" + i].set_visible(false);
			sector.Stage.inventory_item_renderer["s" + i] <- FloatingImage(new_item.sprite);
			Stage.render_inventory_item(i);
		}
	}
}

Stage.inventory_empty <- function() {
	foreach(slot in state.stage.inventory) if(slot) return false;
	return true;
}

Stage.empty_inventory <- function() {
	for(i <- 0; i < 12; ++i) {
		if(!state.stage.inventory["s" + i]) continue;
		sector.Stage.inventory_item_renderer["s" + i].set_visible(false);
		state.stage.inventory["s" + i] <- false;
	}
}

Stage.items_in_inventory <- function(item_name = null) {
	ret <- 0;
	foreach(item in state.stage.inventory) if(item && (!item_name || item.name == item_name)) ++ret;
	return ret;
}

Stage.unique_items_in_inventory <- function() {
	ret <- 0;
	db <- {};
	foreach(item in state.stage.inventory) if(item && !(item.name in db)) {
		db[item.name] <- true;
		++ret;
	}
	return ret;
}

Stage.cancel_crafting <- function() {
	Stage.skip_sound <- true;
	if(Stage.workbench.s0) {
		Stage.collect_item(Stage.StageItem(
			Stage.workbench.s0.name,
			Stage.workbench.s0.sprite,
			Stage.workbench.s0.description)
		);
		Stage.workbench_item_renderer.s0.set_visible(false);
		Stage.workbench.s0 <- false;
	}
	if(Stage.workbench.s1) {
		Stage.collect_item(Stage.StageItem(
			Stage.workbench.s1.name,
			Stage.workbench.s1.sprite,
			Stage.workbench.s1.description)
		);
		Stage.workbench_item_renderer.s1.set_visible(false);
		Stage.workbench.s1 <- false;
	}
	if(Stage.workbench.s2) {
		Stage.workbench_item_renderer.s2.set_visible(false);
		Stage.workbench.s2 <- false;
	}
	Stage.skip_sound <- false;
}

Stage.craft <- function() {
	if(Stage.workbench.s0) {
		Stage.workbench_item_renderer.s0.set_visible(false);
		Stage.workbench.s0 <- false;
	}
	if(Stage.workbench.s1) {
		Stage.workbench_item_renderer.s1.set_visible(false);
		Stage.workbench.s1 <- false;
	}
	if(Stage.workbench.s2) {
		Stage.collect_item(Stage.StageItem(
			Stage.workbench.s2.name,
			Stage.workbench.s2.sprite,
			Stage.workbench.s2.description)
		);
		Stage.update_status(format(_("%s created"), _(Stage.workbench.s2.name)));
		if(Stage.workbench.s2.name in Stage.craft_functions && Stage.craft_functions[Stage.workbench.s2.name]) {
			Stage.inventory_info.set_visible(false);
			Stage.inventory_info2.set_visible(false);
			Stage.tip.set_visible(false);
			sector.Tux.activate();
			Stage.craft_functions[Stage.workbench.s2.name]();
			wait(0);
			sector.Tux.deactivate();
			Stage.inventory_info.set_visible(true);
			Stage.inventory_info2.set_visible(true);
			Stage.tip.set_visible(true);
			Stage.left_caption.set_visible(false);
			Stage.right_caption.set_visible(false);
			Stage.mid_caption.set_visible(false);
			Stage.left_caption_timer <- 0;
			Stage.right_caption_timer <- 0;
			Stage.mid_caption_timer <- 0;
			Stage.left_caption_on <- false;
			Stage.right_caption_on <- false;
			Stage.mid_caption_on <- false;
			Stage.prevent_teleport <- true;
		}

		Stage.workbench_item_renderer.s2.set_visible(false);
		Stage.workbench.s2 <- false;
	}
}

Stage.open_map <- function() {
	local fast_travel = null;
	Stage.inventory_info.set_visible(false);
	Stage.inventory_info2.set_visible(false);
	Stage.update_status(_("Map opened"));

	Stage.mapinfo <- TextObject();
	Stage.mapinfo.set_text(_("Press ACTION or ITEM POCKET to close"));
	Stage.mapinfo.set_anchor_point(ANCHOR_TOP_LEFT);
	Stage.mapinfo.set_text_color(1, 1, 0, 1);
	Stage.mapinfo.set_back_fill_color(0, 0, 0, 0);
	Stage.mapinfo.set_front_fill_color(0, 0, 0, 0);
	Stage.mapinfo.set_pos(10, 18);
	Stage.mapinfo.set_visible(true);
	
	Stage.ftinfo <- TextObject();
	Stage.ftinfo.set_text(_("Press ENTER to fast travel"));
	Stage.ftinfo.set_anchor_point(ANCHOR_TOP_RIGHT);
	Stage.ftinfo.set_text_color(1, 1, 0, 1);
	Stage.ftinfo.set_back_fill_color(0, 0, 0, 0);
	Stage.ftinfo.set_front_fill_color(0, 0, 0, 0);
	Stage.ftinfo.set_pos(-10, 18);

	Stage.crosshair <- FloatingImage("stage/images/crosshair.png");
	Stage.crosshair.set_layer(505);
	Stage.crosshair.set_visible(true);
	Stage.map_piece_info <- TextObject();
	local curpos_x = Level.stage.map_position[0] + 128;
	local curpos_y = Level.stage.map_position[1] + 128;
	if(Level.stage.map[curpos_x][curpos_y]) {
		Stage.map_piece_info.set_text(Level.stage.map[curpos_x][curpos_y].name);
		local cl = Stage.get_colour(
			Level.stage.map[curpos_x][curpos_y].colour
		);
		Stage.map_piece_info.set_text_color(cl[0], cl[1], cl[2], 1);
	}
	if(Level.stage.map[curpos_x][curpos_y].sector && Level.stage.map[curpos_x][curpos_y].spawnpoint) {
		Stage.ftinfo.set_visible(true);
		fast_travel = [Level.stage.map[curpos_x][curpos_y].sector, Level.stage.map[curpos_x][curpos_y].spawnpoint];
	}
	Stage.map_piece_info.set_anchor_point(ANCHOR_BOTTOM);
	Stage.map_piece_info.set_pos(0, -79);
	Stage.map_piece_info.set_font("big");
	Stage.map_piece_info.set_roundness(0);
	Stage.map_piece_info.set_back_fill_color(0.3, 0.3, 0.3, 0.68);
	Stage.map_piece_info.set_front_fill_color(0, 0, 0, 0);
	Stage.map_piece_info.set_visible(true);
	
	if(Stage.tips.len()) Stage.tip.set_visible(false);
	Stage.screen_name_indicator.set_visible(false);
	Stage.inventory_selector.set_visible(false);

	local offset_x = -192 * (Level.stage.map_position[0]);
	local offset_y = -192 * (Level.stage.map_position[1]);

	Stage.map_view <- array(256);
	for(i <- 0; i < 256; ++i) {
		Stage.map_view[i] = array(256);
		for(j <- 0; j < 256; ++j) {
			if(Level.stage.map[i][j]) {
				Stage.map_view[i][j] = FloatingImage(Level.stage.map[i][j].image);
				Stage.map_view[i][j].set_layer(500);
				Stage.map_view[i][j].set_pos((i - 128) * 192 + offset_x, (j - 128) * 192 + offset_y);
				Stage.map_view[i][j].set_visible(true);
			} else Stage.map_view[i][j] = null;
		}
	}
	

	while(sector.Tux.get_input_held("item")) wait(0);

	local current_id = Level.stage.stage_id;

	while(true) {
		try {
			if(!("Stage" in sector) || current_id != Level.stage.stage_id) return;
		} catch(e) {
			return;
		}
		if(sector.Tux.get_input_held("item") || sector.Tux.get_input_held("action")) {
			fast_travel = null;
			break;
		}
		if(sector.Tux.get_input_held("right") && -offset_x < Level.stage.map_extremes.right * 192 + 96) {
			offset_x -= 8;
			for(i <- 0; i < 256; ++i) {
				for(j <- 0; j < 256; ++j) {
					if(Stage.map_view[i][j]) {
						Stage.map_view[i][j].set_pos(
							Stage.map_view[i][j].get_x() - 8,
							Stage.map_view[i][j].get_y()
						);
					}
				}
			}
			local nx = (24576 - offset_x + 96) / 192;
			local ny = (24576 - offset_y + 96) / 192;
			if(-offset_x >= Level.stage.map_extremes.right * 192 + 96) --nx;
			if(-offset_y >= Level.stage.map_extremes.bottom * 192 + 96) --ny;
			if(Level.stage.map[nx][ny]) {
				Stage.map_piece_info.set_text(Level.stage.map[nx][ny].name);
				local nc = Stage.get_colour(Level.stage.map[nx][ny].colour);
				Stage.map_piece_info.set_text_color(nc[0], nc[1], nc[2], 1);
				if(Level.stage.map[nx][ny].sector && Level.stage.map[nx][ny].spawnpoint) {
					Stage.ftinfo.set_visible(true);
					fast_travel = [Level.stage.map[nx][ny].sector, Level.stage.map[nx][ny].spawnpoint];
				} else {
					Stage.ftinfo.set_visible(false);
					fast_travel = null;
				}
			} else {
				Stage.map_piece_info.set_text("");
				Stage.ftinfo.set_visible(false);
				fast_travel = null;
			}
		} else if(sector.Tux.get_input_held("left") && offset_x < -Level.stage.map_extremes.left * 192 + 96) {
			offset_x += 8;
			for(i <- 0; i < 256; ++i) {
				for(j <- 0; j < 256; ++j) {
					if(Stage.map_view[i][j]) {
						Stage.map_view[i][j].set_pos(
							Stage.map_view[i][j].get_x() + 8,
							Stage.map_view[i][j].get_y()
						);
					}
				}
			}
			local nx = (24576 - offset_x + 96) / 192;
			local ny = (24576 - offset_y + 96) / 192;
			if(-offset_x >= Level.stage.map_extremes.right * 192 + 96) --nx;
			if(-offset_y >= Level.stage.map_extremes.bottom * 192 + 96) --ny;
			if(Level.stage.map[nx][ny]) {
				Stage.map_piece_info.set_text(Level.stage.map[nx][ny].name);
				local nc = Stage.get_colour(Level.stage.map[nx][ny].colour);
				Stage.map_piece_info.set_text_color(nc[0], nc[1], nc[2], 1);
				if(Level.stage.map[nx][ny].sector && Level.stage.map[nx][ny].spawnpoint) {
					Stage.ftinfo.set_visible(true);
					fast_travel = [Level.stage.map[nx][ny].sector, Level.stage.map[nx][ny].spawnpoint];
				} else {
					Stage.ftinfo.set_visible(false);
					fast_travel = null;
				}
			} else {
				Stage.map_piece_info.set_text("");
				Stage.ftinfo.set_visible(false);
				fast_travel = null;
			}
		}
		if(sector.Tux.get_input_held("down") && -offset_y < Level.stage.map_extremes.bottom * 192 + 96) {
			offset_y -= 8;
			for(i <- 0; i < 256; ++i) {
				for(j <- 0; j < 256; ++j) {
					if(Stage.map_view[i][j]) {
						Stage.map_view[i][j].set_pos(
							Stage.map_view[i][j].get_x(),
							Stage.map_view[i][j].get_y() - 8
						);
					}
				}
			}
			local nx = (24576 - offset_x + 96) / 192;
			local ny = (24576 - offset_y + 96) / 192;
			if(-offset_x >= Level.stage.map_extremes.right * 192 + 96) --nx;
			if(-offset_y >= Level.stage.map_extremes.bottom * 192 + 96) --ny;
			if(Level.stage.map[nx][ny]) {
				Stage.map_piece_info.set_text(Level.stage.map[nx][ny].name);
				local nc = Stage.get_colour(Level.stage.map[nx][ny].colour);
				Stage.map_piece_info.set_text_color(nc[0], nc[1], nc[2], 1);
				if(Level.stage.map[nx][ny].sector && Level.stage.map[nx][ny].spawnpoint) {
					Stage.ftinfo.set_visible(true);
					fast_travel = [Level.stage.map[nx][ny].sector, Level.stage.map[nx][ny].spawnpoint];
				} else {
					Stage.ftinfo.set_visible(false);
					fast_travel = null;
				}
			} else {
				Stage.map_piece_info.set_text("");
				Stage.ftinfo.set_visible(false);
				fast_travel = null;
			}
		} else if(sector.Tux.get_input_held("up") && offset_y < -Level.stage.map_extremes.top * 192 + 96) {
			offset_y += 8;
			for(i <- 0; i < 256; ++i) {
				for(j <- 0; j < 256; ++j) {
					if(Stage.map_view[i][j]) {
						Stage.map_view[i][j].set_pos(
							Stage.map_view[i][j].get_x(),
							Stage.map_view[i][j].get_y() + 8
						);
					}
				}
			}
			local nx = (24576 - offset_x + 96) / 192;
			local ny = (24576 - offset_y + 96) / 192;
			if(-offset_x >= Level.stage.map_extremes.right * 192 + 96) --nx;
			if(-offset_y >= Level.stage.map_extremes.bottom * 192 + 96) --ny;
			if(Level.stage.map[nx][ny]) {
				Stage.map_piece_info.set_text(Level.stage.map[nx][ny].name);
				local nc = Stage.get_colour(Level.stage.map[nx][ny].colour);
				Stage.map_piece_info.set_text_color(nc[0], nc[1], nc[2], 1);
				if(Level.stage.map[nx][ny].sector && Level.stage.map[nx][ny].spawnpoint) {
					Stage.ftinfo.set_visible(true);
					fast_travel = [Level.stage.map[nx][ny].sector, Level.stage.map[nx][ny].spawnpoint];
				} else {
					Stage.ftinfo.set_visible(false);
					fast_travel = null;
				}
			} else {
				Stage.map_piece_info.set_text("");
				Stage.ftinfo.set_visible(false);
				fast_travel = null;
			}
		} else if(sector.Tux.get_input_held("menu-select")) {
			if(fast_travel) break;
		}
		wait(0);
	}

	for(i <- 0; i < 256; ++i) for(j <- 0; j < 256; ++j) if(Stage.map_view[i][j]) Stage.map_view[i][j].set_visible(false);
	Stage.inventory_info.set_visible(true);
	Stage.inventory_info2.set_visible(true);
	Stage.mapinfo.set_visible(false);
	Stage.crosshair.set_visible(false);
	Stage.map_piece_info.set_visible(false);
	Stage.ftinfo.set_visible(false);
	Stage.screen_name_indicator.set_visible(true);
	if(Stage.tips.len()) Stage.tip.set_visible(true);
	Stage.inventory_selector.set_visible(true);
	Stage.update_status(_("Map closed"));
	while(sector.Tux.get_input_held("item") || sector.Tux.get_input_held("action")) wait(0);
	if(fast_travel) Stage.skip_inventory_tp <- fast_travel;
}

Stage.open_inventory <- function() {
	sector.Tux.deactivate();

	Stage.prevent_teleport <- true;
	Stage.inventory_open <- true;

	Stage.left_caption.set_visible(false);
	Stage.right_caption.set_visible(false);
	Stage.mid_caption.set_visible(false);
	Stage.left_caption_timer <- 0;
	Stage.right_caption_timer <- 0;
	Stage.mid_caption_timer <- 0;
	Stage.left_caption_on <- false;
	Stage.right_caption_on <- false;
	Stage.mid_caption_on <- false;

	Stage.inventory_info <- TextObject();
	Stage.inventory_info.set_wrap_width(10000);
	Stage.inventory_info.set_roundness(0);
	Stage.inventory_info.set_back_fill_color(0.7, 0, 0, 1);
	Stage.inventory_info.set_front_fill_color(0.4, 0.4, 0.4, 1);
	Stage.inventory_info.set_anchor_point(ANCHOR_RIGHT);
	Stage.inventory_info.set_pos(-54 - sector.Camera.get_screen_width() / 2, 0);
	Stage.inventory_info.set_text(_(
		_("MOVEMENT KEYS") + "\n" +
		_("ENTER") + "\n" +
		_("JUMP") + "\n" +
		_("PEEK UP") + "\n" +
		_("PEEK RIGHT") + "\n" +
		_("PEEK DOWN") + "\n" +
		_("PEEK LEFT") + "\n" +
		_("ACTION")
	));
	if(Level.stage.map) Stage.inventory_info.set_text(Stage.inventory_info.get_text() + "\n" + _("ITEM POCKET"));

	Stage.inventory_info2 <- TextObject();
	Stage.inventory_info2.set_wrap_width(10000);
	Stage.inventory_info2.set_roundness(0);
	Stage.inventory_info2.set_back_fill_color(0.7, 0, 0, 1);
	Stage.inventory_info2.set_front_fill_color(0.4, 0.4, 0.4, 1);
	Stage.inventory_info2.set_anchor_point(ANCHOR_LEFT);
	Stage.inventory_info2.set_pos(sector.Camera.get_screen_width() / 2 - 54, 0);
	Stage.inventory_info2.set_text(_(
		_("select an item") + "\n" +
		_("use/drop selected item") + "\n" +
		_("move selected item") + "\n" +
		_("examine selected item") + "\n" +
		_("craft with selected item") + "\n" +
		_("confirm crafting") + "\n" +
		_("cancel crafting") + "\n" +
		_("close inventory")
	));
	if(Level.stage.map) Stage.inventory_info2.set_text(Stage.inventory_info2.get_text() + "\n" + _("open map"));
	Stage.inventory_info.fade_in(0.25);
	Stage.inventory_info2.fade_in(0.25);

	Stage.inventory_selector <- FloatingImage("/stage/images/select.png");
	Stage.inventory_selector.set_anchor_point(ANCHOR_BOTTOM);
	Stage.inventory_selector.set_pos(-217 / 2 + 16, 1 -37);
	Stage.inventory_selector.set_layer(1010);
	Stage.inventory_selector.set_visible(true);

	Stage.tip <- TextObject();
	Stage.tip.set_wrap_width(sector.Camera.get_screen_width());
	if(Stage.tips.len()) {
		Stage.tip.set_text(format(_("Tip: %s"), _(Stage.tips[rand() % Stage.tips.len()])));
		Stage.tip.set_anchor_point(ANCHOR_BOTTOM);
		Stage.tip.set_pos(0, -79);
		Stage.tip.set_font("small");
		Stage.tip.set_roundness(0);
		Stage.tip.set_back_fill_color(0.7, 0, 0, 1);
		Stage.tip.set_front_fill_color(0.4, 0.4, 0.4, 1);
		Stage.tip.set_text_color(1, 1, 0, 1);
		Stage.tip.fade_in(0.25);
	}

	Stage.selected_item <- 0;
	if(state.stage.inventory["s" + Stage.selected_item])
		Stage.update_status(format(_("%s selected"), _(state.stage.inventory["s" + Stage.selected_item].name)));
	else Stage.update_status(_("Inventory opened"));
	wait(0.25);
	Stage.cycles_held <- 0;
	Stage.last_direction_held <- "";
	Stage.item_drop <- false;

	while(sector.Tux.get_input_held("menu-select")) wait(0.01);

	local current_id = Level.stage.stage_id;

	while(true) {
		wait(0);
		try {
			if(!("Stage" in sector) || current_id != Level.stage.stage_id) return;
		} catch(e) {
			return;
		}
		Stage.call_function <- null;
		Stage.call_function_singlethread <- null;
		if(sector.Tux.get_input_held("action")) break;
		else if(sector.Tux.get_input_held("up")) {
			if(Stage.selected_item < 6) continue;
			Stage.selected_item -= 6;
			if(state.stage.inventory["s" + Stage.selected_item])
				Stage.update_status(format(_("%s selected"), _(state.stage.inventory["s" + Stage.selected_item].name)));
			else Stage.update_status("");
			if(Stage.last_direction_held != "up") Stage.cycles_held <- 0;
			Stage.last_direction_held <- "up";
		} else if(sector.Tux.get_input_held("down")) {
			if(Stage.selected_item > 5) continue;
			Stage.selected_item += 6;
			if(state.stage.inventory["s" + Stage.selected_item])
				Stage.update_status(format(_("%s selected"), _(state.stage.inventory["s" + Stage.selected_item].name)));
			else Stage.update_status("");
			if(Stage.last_direction_held != "down") Stage.cycles_held <- 0;
			Stage.last_direction_held <- "down";
		} else if(sector.Tux.get_input_held("left")) {
			if(!(Stage.selected_item % 6)) continue;
			--Stage.selected_item;
			if(state.stage.inventory["s" + Stage.selected_item])
				Stage.update_status(format(_("%s selected"), _(state.stage.inventory["s" + Stage.selected_item].name)));
			else Stage.update_status("");
			if(Stage.last_direction_held != "left") Stage.cycles_held <- 0;
			Stage.last_direction_held <- "left";
		} else if(sector.Tux.get_input_held("right")) {
			if(Stage.selected_item % 6 == 5) continue;
			++Stage.selected_item;
			if(state.stage.inventory["s" + Stage.selected_item])
				Stage.update_status(format(_("%s selected"), _(state.stage.inventory["s" + Stage.selected_item].name)));
			else Stage.update_status("");
			if(Stage.last_direction_held != "right") Stage.cycles_held <- 0;
			Stage.last_direction_held <- "right";
		} else if(sector.Tux.get_input_held("menu-select")) {
			if(!state.stage.inventory["s" + Stage.selected_item]) continue;
			if(state.stage.inventory["s" + Stage.selected_item].name in sector.Stage.accept_items &&
			   sector.Stage.accept_items[state.stage.inventory["s" + Stage.selected_item].name]) {
				Stage.update_status(format(_("%s used"), _(state.stage.inventory["s" + Stage.selected_item].name)));
				Stage.item_drop <- true;
				Stage.use_function <- sector.Stage.accept_items[state.stage.inventory["s" + Stage.selected_item]["name"]][0];
				if(!sector.Stage.accept_items[state.stage.inventory["s" + Stage.selected_item]["name"]][1]) {
					state.stage.inventory["s" + Stage.selected_item] <- false;
					sector.Stage.inventory_item_renderer["s" + Stage.selected_item].set_visible(false);
				}
				Stage.call_function <- newthread(Stage.use_function);
				Stage.call_function_singlethread <- Stage.use_function;
				break;
			} else {
				if(state.stage.inventory["s" + Stage.selected_item].name in sector.Stage.prevent_drop_functions &&
				   sector.Stage.prevent_drop_functions[state.stage.inventory["s" + Stage.selected_item].name]) {
					Stage.call_function <- newthread(
						sector.Stage.prevent_drop_functions[state.stage.inventory["s" + Stage.selected_item].name]
					);
					Stage.call_function_singlethread <- sector.Stage.prevent_drop_functions[
						                              state.stage.inventory["s" + Stage.selected_item].name
						                            ];
					break;
				}
				Stage.update_status(format(_("%s dropped"), _(state.stage.inventory["s" + Stage.selected_item].name)));
				Stage.item_drop <- true;
				drop_fnc <- null;
				if(state.stage.inventory["s" + Stage.selected_item].name in sector.Stage.drop_functions &&
				   sector.Stage.drop_functions[state.stage.inventory["s" + Stage.selected_item].name]) {
					Stage.call_function <- newthread(
						sector.Stage.drop_functions[state.stage.inventory["s" + Stage.selected_item].name]
					);
					Stage.call_function_singlethread <- sector.Stage.drop_functions[
					                                      state.stage.inventory["s" + Stage.selected_item].name
					                                    ];
				}
				if(endswith(sector.Tux.get_action(), "left")) {
					if(sector.settings.is_free_of_statics(
					   sector.Tux.get_x() - 32, sector.Tux.get_y(),
					   sector.Tux.get_x() - 4,  sector.Tux.get_y() + 16, true) &&
					   sector.Tux.get_x() >= 20) {
						Stage.spawn_item(
							state.stage.inventory["s" + Stage.selected_item],
							sector.Tux.get_x() - 36, sector.Tux.get_y()
						);
					} else if(sector.settings.is_free_of_statics(
					          sector.Tux.get_x() - 32, sector.Tux.get_y() - 28,
					          sector.Tux.get_x() - 4,  sector.Tux.get_y() - 12, true) &&
					          sector.Tux.get_x() >= 20) {
						Stage.spawn_item(
							state.stage.inventory["s" + Stage.selected_item],
							sector.Tux.get_x() - 36, sector.Tux.get_y() - 32
						);
					} else if(sector.settings.is_free_of_statics(
					          sector.Tux.get_x() + 36, sector.Tux.get_y(),
					          sector.Tux.get_x() + 64, sector.Tux.get_y() + 16, true) &&
					          (sector.Tux.get_x() < (sector.Camera.get_x() + sector.Camera.get_screen_width() - 52))) {
						Stage.spawn_item(
							state.stage.inventory["s" + Stage.selected_item],
							sector.Tux.get_x() + 36, sector.Tux.get_y()
						);
					} else if(sector.settings.is_free_of_statics(
					          sector.Tux.get_x() + 36, sector.Tux.get_y() - 28,
					          sector.Tux.get_x() + 64, sector.Tux.get_y() - 12, true) &&
					          (sector.Tux.get_x() < (sector.Camera.get_x() + sector.Camera.get_screen_width() - 52))) {
						Stage.spawn_item(
							state.stage.inventory["s" + Stage.selected_item],
							sector.Tux.get_x() + 36, sector.Tux.get_y() - 32
						);
					} else {
						Stage.update_status(_("Cannot drop item"));
						break;
					}
				} else {
					if(sector.settings.is_free_of_statics(
					   sector.Tux.get_x() + 36, sector.Tux.get_y(),
					   sector.Tux.get_x() + 64, sector.Tux.get_y() + 16, true) && 
					   (sector.Tux.get_x() < (sector.Camera.get_x() + sector.Camera.get_screen_width() - 52))) {
						Stage.spawn_item(
							state.stage.inventory["s" + Stage.selected_item],
							sector.Tux.get_x() + 36, sector.Tux.get_y()
						);
					} else if(sector.settings.is_free_of_statics(
					          sector.Tux.get_x() + 36, sector.Tux.get_y() - 28,
					          sector.Tux.get_x() + 64, sector.Tux.get_y() - 12, true) &&
					          (sector.Tux.get_x() < (sector.Camera.get_x() + sector.Camera.get_screen_width() - 52))) {
						Stage.spawn_item(
							state.stage.inventory["s" + Stage.selected_item],
							sector.Tux.get_x() + 36, sector.Tux.get_y() - 32
						);
					} else if(sector.settings.is_free_of_statics(
					          sector.Tux.get_x() - 32, sector.Tux.get_y(),
					          sector.Tux.get_x() - 4,  sector.Tux.get_y() + 16, true) &&
					          sector.Tux.get_x() >= 20) {
						Stage.spawn_item(
							state.stage.inventory["s" + Stage.selected_item],
							sector.Tux.get_x() - 36, sector.Tux.get_y()
						);
					} else if(sector.settings.is_free_of_statics(
					          sector.Tux.get_x() - 32, sector.Tux.get_y() - 28,
					          sector.Tux.get_x() - 4, sector.Tux.get_y() - 12, true) &&
					          sector.Tux.get_x() >= 20) {
						Stage.spawn_item(
							state.stage.inventory["s" + Stage.selected_item],
							sector.Tux.get_x() - 36, sector.Tux.get_y() - 32
						);
					} else {
						Stage.update_status(_("Cannot drop item"));
						break;
					}
				}
				state.stage.inventory["s" + Stage.selected_item] <- false;
				sector.Stage.inventory_item_renderer["s" + Stage.selected_item].set_visible(false);
				break;
			}
		} else if(sector.Tux.get_input_held("peek-up")) {
			if(!state.stage.inventory["s" + Stage.selected_item] ||
			   !state.stage.inventory["s" + Stage.selected_item].description) continue;
			Stage.update_status(format(_("%s examined"), _(state.stage.inventory["s" + Stage.selected_item].name)));
			Stage.inventory_info.set_visible(false);
			Stage.inventory_info2.set_visible(false);
			Stage.tip.set_visible(false);
			sector.Tux.activate();
			if(state.stage.inventory["s" + Stage.selected_item].name in sector.Stage.examine_functions &&
			   sector.Stage.examine_functions[state.stage.inventory["s" + Stage.selected_item].name]) {
				try {
					sector.Stage.examine_functions[state.stage.inventory["s" + Stage.selected_item].name]();
					if(!("Stage" in sector) || current_id != Level.stage.stage_id) return;
				} catch(e) {
					return;
				}
				wait(0);
				Stage.left_caption.set_visible(false);
				Stage.right_caption.set_visible(false);
				Stage.mid_caption.set_visible(false);
				Stage.left_caption_timer <- 0;
				Stage.right_caption_timer <- 0;
				Stage.mid_caption_timer <- 0;
				Stage.left_caption_on <- false;
				Stage.right_caption_on <- false;
				Stage.mid_caption_on <- false;
				Stage.prevent_teleport <- true;
			} else {
				try {
					Stage.dialogue([[state.stage.inventory["s" + Stage.selected_item].description, [0.7, 0.7, 0.7]]]);
					if(!("Stage" in sector) || current_id != Level.stage.stage_id) return;
				} catch(e) {
					return;
				}
			}
			sector.Tux.deactivate();
			Stage.tip.set_visible(true);
			Stage.inventory_info.set_visible(true);
			Stage.inventory_info2.set_visible(true);
			Stage.update_status(format(_("%s selected"), _(state.stage.inventory["s" + Stage.selected_item].name)));
			while(sector.Tux.get_input_held("peek-up")) wait(0.01);
			continue;
		} else if(sector.Tux.get_input_held("peek-right")) {
			if(!state.stage.inventory["s" + Stage.selected_item] ||
			   (Stage.workbench.s0 && Stage.workbench.s1)) continue;
			Stage.working_index <- Stage.workbench.s0 ? "s1" : "s0";
			Stage.workbench[Stage.working_index] <- Stage.StageItem(
				state.stage.inventory["s" + Stage.selected_item].name,
				state.stage.inventory["s" + Stage.selected_item].sprite,
				state.stage.inventory["s" + Stage.selected_item].description
			);

			Stage.workbench_item_renderer[Stage.working_index] <- FloatingImage(Stage.workbench[Stage.working_index].sprite);
			Stage.render_workbench_item(Stage.working_index == "s0" ? 0 : 1);

			state.stage.inventory["s" + Stage.selected_item] <- false;
			sector.Stage.inventory_item_renderer["s" + Stage.selected_item].set_visible(false);

			if(Stage.workbench.s0 && Stage.workbench.s1) {
				foreach(test in [Stage.workbench.s0.name + "+" + Stage.workbench.s1.name,
				        Stage.workbench.s1.name + "+" + Stage.workbench.s0.name]) {
						if(test in Stage.recipes) {
							Stage.workbench.s2 <- Stage.StageItem(
								Stage.recipes[test].name,
								Stage.recipes[test].sprite,
								Stage.recipes[test].description
							);
							break;
						}
				}
				if(Stage.workbench.s2) {
					Stage.workbench_item_renderer["s2"] <- FloatingImage(Stage.workbench.s2.sprite);
					Stage.render_workbench_item(2);
				}
			}
			while(sector.Tux.get_input_held("peek-right")) wait(0.01);
			continue;
		} else if(sector.Tux.get_input_held("peek-left")) {
			if(Stage.workbench.s0) {
				Stage.cancel_crafting();
				Stage.update_status(_("Crafting cancelled"));
			}
			continue;
		} else if(sector.Tux.get_input_held("peek-down")) {
			if(Stage.workbench.s2) Stage.craft();
			continue;
		} else if(Level.stage.map && sector.Tux.get_input_held("item")) {
			Stage.open_map();
			if(Stage.skip_inventory_tp) break;
			continue;
		} else if(sector.Tux.get_input_held("jump")) {
			if(!state.stage.inventory["s" + Stage.selected_item]) {
				Stage.update_status(_("No item selected"));
				continue;
			}

			while(sector.Tux.get_input_held("jump")) wait(0.01)

			Stage.move_close <- false;
			Stage.original_pos <- Stage.selected_item;
			Stage.last_input_held <- "";
			Stage.held_item <- Stage.StageItem(
				state.stage.inventory["s" + Stage.selected_item].name,
				state.stage.inventory["s" + Stage.selected_item].sprite,
				state.stage.inventory["s" + Stage.selected_item].description
			);
			Stage.update_status(format("%s held", _(state.stage.inventory["s" + Stage.selected_item].name)));
			Stage.inventory_selector.set_visible(false);

			Stage.inventory_mover <- FloatingImage("/stage/images/move.png");
			Stage.inventory_mover.set_anchor_point(ANCHOR_BOTTOM);
			Stage.inventory_mover.set_pos(
				-217 / 2 + (5 + 32) * (Stage.selected_item % 6) + 16,
				1 - (Stage.selected_item < 6 ? 37 : 0)
			);
			Stage.inventory_mover.set_layer(1010);
			Stage.inventory_mover.set_visible(true);

			Stage.held_item_fi <- FloatingImage(state.stage.inventory["s" + Stage.selected_item].sprite);
			Stage.held_item_fi.set_anchor_point(ANCHOR_BOTTOM);
			Stage.held_item_fi.set_pos(
				-217 / 2 + (5 + 32) * (Stage.selected_item % 6) + 16,
				-2 - (Stage.selected_item < 6 ? 37 : 0)
			);
			Stage.held_item_fi.set_layer(1008);
			Stage.held_item_fi.set_visible(true);

			sector.Stage.inventory_item_renderer["s" + Stage.selected_item].set_visible(false);

			while(true) {
				wait(0.01);
				if(sector.Tux.get_input_held("action") || (Level.stage.map && sector.Tux.get_input_held("item"))) {
					sector.Stage.inventory_item_renderer["s" + Stage.original_pos].set_visible(true);
					Stage.inventory_mover.set_visible(false);
					Stage.held_item_fi.set_visible(false);
					Stage.move_close = true;
					break;
				} else if(sector.Tux.get_input_held("jump") && !sector.Tux.get_input_held("up")) {
					state.stage.inventory["s" + Stage.original_pos] <- false;
					if(state.stage.inventory["s" + Stage.selected_item]) {
						sector.Stage.inventory_item_renderer["s" + Stage.selected_item].set_visible(false);
						state.stage.inventory["s" + Stage.original_pos] <- Stage.StageItem(
							state.stage.inventory["s" + Stage.selected_item].name,
							state.stage.inventory["s" + Stage.selected_item].sprite,
							state.stage.inventory["s" + Stage.selected_item].description
						);
						sector.Stage.inventory_item_renderer["s" + Stage.original_pos] <- FloatingImage(
							state.stage.inventory["s" + Stage.original_pos].sprite
						);
						Stage.render_inventory_item(Stage.original_pos);
					}
					state.stage.inventory["s" + Stage.selected_item] <- Stage.StageItem(
						Stage.held_item.name,
						Stage.held_item.sprite,
						Stage.held_item.description
					);
					sector.Stage.inventory_item_renderer["s" + Stage.selected_item] <- FloatingImage(
						state.stage.inventory["s" + Stage.selected_item].sprite
					);
					Stage.render_inventory_item(Stage.selected_item);
					Stage.held_item_fi.set_visible(false);
					Stage.inventory_mover.set_visible(false);

					Stage.inventory_selector.set_pos(
						-217 / 2 + (5 + 32) * (Stage.selected_item % 6) + 16,
						1 - (Stage.selected_item < 6 ? 37 : 0)
					);
					Stage.inventory_selector.set_visible(true);

					Stage.update_status(format("%s placed", _(Stage.held_item.name)));
					break;
				} else if(sector.Tux.get_input_held("up")) {
					Stage.cycles_held <- 0;
					Stage.last_input_held <- "up";
					if(Stage.selected_item < 6) continue;
					Stage.selected_item -= 6;

				} else if(sector.Tux.get_input_held("down")) {
					Stage.cycles_held <- 0;
					Stage.last_input_held <- "down";
					if(Stage.selected_item > 5) continue;
					Stage.selected_item += 6;

				} else if(sector.Tux.get_input_held("left")) {
					if(Stage.last_input_held != "left") Stage.cycles_held <- 0;
					Stage.last_input_held <- "left";
					if(!(Stage.selected_item % 6)) continue;
					--Stage.selected_item;

				} else if(sector.Tux.get_input_held("right")) {
					if(Stage.last_input_held != "right") Stage.cycles_held <- 0;
					Stage.last_input_held <- "right";
					if(Stage.selected_item % 6 == 5) continue;
					++Stage.selected_item;
				} else Stage.cycles_held <- 0;
				Stage.inventory_mover.set_pos(
					-217 / 2 + (5 + 32) * (Stage.selected_item % 6) + 16,
					1 - (Stage.selected_item < 6 ? 37 : 0)
				);
				Stage.inventory_selector.set_pos(
					-217 / 2 + (5 + 32) * (Stage.selected_item % 6) + 16,
					1 - (Stage.selected_item < 6 ? 37 : 0)
				);
				Stage.held_item_fi.set_pos(
					-217 / 2 + (5 + 32) * (Stage.selected_item % 6) + 16,
					-2 - (Stage.selected_item < 6 ? 37 : 0)
				);
				while(sector.Tux.get_input_held("left") || sector.Tux.get_input_held("right")) {
					if(sector.Tux.get_input_held("left")) Stage.last_input_held <- "left";
					else Stage.last_input_held <- "right";
					wait(0.01);
					if(++Stage.cycles_held >= 40) break;
				}
			}
			if(Stage.move_close) {
				if(Level.stage.map && sector.Tux.get_input_held("item")) continue;
				break;
			}
			while(sector.Tux.get_input_held("jump")) wait(0.01);
			continue;
		} else {
			Stage.cycles_held <- 0;
			continue;
		}
		Stage.inventory_selector.set_pos(-217 / 2 + (5 + 32) * (Stage.selected_item % 6) + 16, 1 - (Stage.selected_item < 6 ? 37 : 0));
		while(true) {
			wait(0.01);
			if(!sector.Tux.get_input_held(Stage.last_direction_held)) {
				Stage.cycles_held <- 0;
				break;
			}
			if(++Stage.cycles_held >= 40) break;
		}
	}

	Stage.cancel_crafting();
	if(!Stage.item_drop) Stage.update_status(_("Inventory closed"));

	Stage.inventory_selector.set_visible(false);
	Stage.inventory_info.fade_out(0.25);
	Stage.inventory_info2.fade_out(0.25);
	Stage.tip.fade_out(0.25);
	wait(0.15);
	sector.Tux.activate();

	Stage.prevent_teleport <- false;
	Stage.inventory_open <- false;

	if(Stage.call_function) {
		if(Stage.multithreading) Stage.call_function.call();
		else Stage.call_function_singlethread();
	}

	if(Stage.skip_inventory_tp) {
		if(!Stage.item_drop) Stage.update_status(_("Fast travel used"));
		Stage.teleport(Stage.skip_inventory_tp[0], Stage.skip_inventory_tp[1]);
	}

	while(sector.Tux.get_input_held("menu-select") || sector.Tux.get_input_held("action")) wait(0.01);
}

Stage.leave_sector <- function() {
	for(i <- 0; i < 12; ++i) {
		if(("s" + i) in sector.Stage.inventory_item_renderer) {
			sector.Stage.inventory_item_renderer["s" + i].set_visible(false);
		}
	}
	Stage.inventory_background.set_visible(false);
	Stage.workbench_background.set_visible(false);
	Stage.screen_name_indicator.set_visible(false);
	Stage.status_bar.set_visible(false);

	Stage.left_caption_on <- false;
	Stage.right_caption_on <- false;
	Stage.mid_caption_on <- false;

	Stage.left_caption_timer <- 0;
	Stage.right_caption_timer <- 0;
	Stage.mid_caption_timer <- 0;

	Stage.left_caption.set_visible(false);
	Stage.right_caption.set_visible(false);
	Stage.mid_caption.set_visible(false);
}

Stage.render_inventory_item <- function(index) {
	sector.Stage.inventory_item_renderer["s" + index].set_anchor_point(ANCHOR_BOTTOM);
	sector.Stage.inventory_item_renderer["s" + index].set_pos(-217 / 2 + (5 + 32) * (index % 6) + 16, -2 - (index < 6 ? 37 : 0));
	sector.Stage.inventory_item_renderer["s" + index].set_layer(1005);
	sector.Stage.inventory_item_renderer["s" + index].set_visible(true);
}

Stage.render_workbench_item <- function(index) {
	Stage.workbench_item_renderer["s" + index].set_anchor_point(ANCHOR_BOTTOM_RIGHT);
	Stage.workbench_item_renderer["s" + index].set_pos(-52 - (2 - index) * 64 + index * 10, -21);
	Stage.workbench_item_renderer["s" + index].set_layer(1005);
	Stage.workbench_item_renderer["s" + index].set_visible(true);
}

Stage.spawn_item <- function(item, x, y) {
	sector.settings.add_object(
		"powerup", "", x, y, "",
		"(disable-physics #f)(sprite \"" + item.sprite +
		"\")(script \"sector.Stage.collect_item(sector.Stage.StageItem(\\\"" +
		item.name + "\\\", \\\"" + item.sprite + "\\\", \\\"" +
		escape(escape(item.description)) + "\\\"))\")"
	);
}

Stage.collect_item <- function(new_item) {
	i <- -1;
	for(i <- 0; i < 12; ++i) {
		if(!state.stage.inventory["s" + i]) {
			if(!Stage.skip_sound) play_sound("sounds/crystallo-pop.ogg");
			state.stage.inventory["s" + i] <- new_item;
			sector.Stage.inventory_item_renderer["s" + i].set_visible(false);
			sector.Stage.inventory_item_renderer["s" + i] <- FloatingImage(new_item.sprite);
			Stage.render_inventory_item(i);
			Stage.update_status(format(_("%s collected"), _(new_item.name)));

			if(new_item.name in Stage.collect_functions && Stage.collect_functions[new_item.name])
				Stage.collect_functions[new_item.name]();
			return;
		}
	}
	Stage.update_status(_("Inventory full!"));
	Stage.spawn_item(new_item, sector.Tux.get_x(), sector.Tux.get_y() - 48);
}

Stage.teleport <- function(sector, spawnpoint) {
	if(Stage.prevent_teleport) return;
	Stage.leave_sector();
	Level.spawn(sector, spawnpoint);
}

Stage.door <- function(sector, spawnpoint) {
	if(Stage.prevent_teleport) return;
	Tux.deactivate();
	Stage.dialogue_open <- true;
	wait(0.7);
	Stage.dialogue_open <- false;
	Tux.activate();
	Stage.teleport(sector, spawnpoint);
}

Stage.allow_inventory <- function(allow_bool) {
	Stage.dialogue_open <- !allow_bool;
}

Stage.allow_teleport <- function(allow_bool) {
	Stage.prevent_teleport <- !allow_bool;
}

Stage.enable_multithreading <- function(allow_bool) {
	Stage.multithreading <- allow_bool;
}

Stage.StageItem <- function(name, sprite, description) {
	ret <- {};
	ret.name <- name;
	ret.sprite <- sprite;
	ret.description <- description;

	return ret;
}

Stage.MapPiece <- function(name, colour, image, sector, spawnpoint) {
	mpp <- {};
	mpp.name <- name;
	mpp.colour <- colour;
	mpp.image <- image;
	mpp.sector <- sector;
	mpp.spawnpoint <- spawnpoint;

	return mpp;
}
